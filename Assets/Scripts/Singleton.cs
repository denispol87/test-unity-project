﻿using UnityEngine;
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance;
    public void Awake()
    {
        if (Instance != null)
        {
            return;
        }
        else
        {
            Instance = this as T;
        }               
    }       
}