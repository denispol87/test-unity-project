﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] protected Image image;
    [SerializeField] protected Text text;
    [SerializeField] public bool HideZero = false;
    public float Value { get { return value; } set {
            this.value = value;
            if (this.value < minValue)
                this.value = minValue;
            if (this.value > maxValue)
                this.value = maxValue;

            if (image != null)                
                image.fillAmount = (this.value - minValue) / (maxValue - minValue);

            if (text != null)
                text.text = this.value.ToString();
            
            gameObject.SetActive(!(this.value == minValue && HideZero));
        } }
    private float value = 0;
    private float minValue = 0;
    private float maxValue = 100;
    public void Init(float value, float minValue = 0, float maxValue = 100)
    {
        this.minValue = minValue;
        this.maxValue = maxValue;
        Value = value;        
    }
    public int GetPercent()
    {
        return (int)(image.fillAmount * 100f);
    }
    public void SetText(string str)
    {
        text.text = str;
    }   
    public void SetTextAnimator(float value)
    {
        text.gameObject.SetActive(false);
        TextAnimator.Instance.SetAnimateText(text, value < 0 ? Color.red : Color.green, value > 0 ? "+" + value.ToString("F0") : value.ToString("F0"));
    }
}
