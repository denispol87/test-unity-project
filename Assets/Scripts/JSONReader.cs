﻿using UnityEngine;

public class JSONReader
{    
    public static T GetJsonData<T>(string path)
    {
        T data = JsonUtility.FromJson<T>(path);
        return data;       
    }
}