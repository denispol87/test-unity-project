﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Main : MonoBehaviour
{
    [SerializeField] public CamMoveController camMoveController;
    [SerializeField] public TextAsset jsonFile;
    [SerializeField] public StatComponent StatPrefab;
    [SerializeField] public Player[] Players;    

    private Pool<StatComponent> statPool;
    private Data data;

    void Start()
    {
        data = JSONReader.GetJsonData<Data>(jsonFile.text);
        camMoveController.SetSettings(data.cameraSettings);
        statPool = new Pool<StatComponent>(transform, StatPrefab.gameObject);
        Restart(true);
    }

    public void Restart(bool buff)
    {
        statPool.DeleteAll();
        for (int i = 0; i < Players.Length; i++)
        {
            List<Stat> stats = GetStats();
            List<Buff> buffs = buff ? GetBuffs(stats) : new List<Buff>();
            Players[i].RestartSettings(Players[Players.Length - 1 - i], stats, buffs,
            ShowStatBuff(Players[i].StatBuffContent, stats, buffs));
        }
    }
    private StatComponent ShowStatBuff(Transform panel, List<Stat> stats, List<Buff> buffs)
    {
        StatComponent hp = null;
        foreach (var stat in stats)
        {
            StatComponent statComponent = statPool.Get(panel);
            statComponent.SetImage(LoadFile.LoadSprite("Icons/" + stat.icon));
            statComponent.SetText(stat.value.ToString());
            if (stat.id == 0)
                hp = statComponent;
        }
        foreach (var buff in buffs)
        {
            StatComponent statComponent = statPool.Get(panel);
            statComponent.SetImage(LoadFile.LoadSprite("Icons/" + buff.icon));
            statComponent.SetText(buff.title);
        }
        return hp;
    }
    private List<Stat> GetStats()
    {
        List<Stat> stats = new List<Stat>();
        for (int i = 0; i < data.stats.Length; i++)
        {
            stats.Add(data.stats[i].Clone());
        }
        return stats;
    }
    private List<Buff> GetBuffs(List<Stat> stats)
    {
        List<Buff> buffs = new List<Buff>();
        do
        {
            if (data.settings.allowDuplicateBuffs)
            {
                for (int i = 0; i < data.settings.buffCountMax; i++)
                {
                    if (Random.value < 0.5)
                    {
                        buffs.Add(data.buffs[Random.Range(0, data.buffs.Length - 1)].Clone());
                        foreach (var stat in buffs.Last().stats)                        
                            stats.First(x => x.id == stat.statId).value += stat.value;                       
                    }
                    if (buffs.Count == data.settings.buffCountMax)
                        break;
                }
            }
            else
            {
                for (int i = 0; i < data.buffs.Length; i++)
                {
                    if (Random.value < 0.5)
                    {
                        buffs.Add(data.buffs[i].Clone());
                        foreach (var stat in buffs.Last().stats)                        
                            stats.First(x => x.id == stat.statId).value += stat.value;                        
                    }
                    if (buffs.Count == data.settings.buffCountMax)
                        break;
                }
            }
        }
        while (buffs.Count < data.settings.buffCountMin);

        return buffs;
    }
}
