﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMoveController : MonoBehaviour
{
    //Вращение камеры по окружности без использования sin и cos
    //очень актуально если на сцене должно быть много объектов вращающихся по кругу
    //данное решение не использует сложных математических вычислений
    [SerializeField] private Vector3 CenterRotation;
    [SerializeField] private float height = 1.5f;
    [SerializeField] private float roundRadius = 1f;
    [SerializeField] private float roamingRadius = 1f;
    [SerializeField] private float speedRotation = 0.005f;
    
    private float roundRadiusStart = 1f;
    private float roundDuration = 15f; 
    private float roamingDuration = 5f; 
    private float fovMin = 35;
    private float fovMax = 75;
    private float fovDelay = 4;
    private float fovDuration = 0.3f;
    
    private float totalTimeX = 0f;
    private float totalTimeY = 0f;
    private float totalTimeZ = 15f / 4f;  
    private float totalTimeFov = 0f;
    private float totalTimeFovDelay = 0f;

    private float randomFov = 0;
    public void SetSettings(CameraModel cameraSettings)
    {
        roundDuration = cameraSettings.roundDuration;
        totalTimeZ = roundDuration / 4f;
        roundRadiusStart = roundRadius = cameraSettings.roundRadius;
        height = cameraSettings.height;
        CenterRotation.y = cameraSettings.lookAtHeight;
        roamingRadius = cameraSettings.roamingRadius;
        fovMin = cameraSettings.fovMin / 100;
        fovMax = cameraSettings.fovMax / 100;
        fovDelay = cameraSettings.fovDelay;
        fovDuration = cameraSettings.fovDuration;
        randomFov = roundRadiusStart * UnityEngine.Random.Range(fovMin, fovMax);
    }
    
    private void Update()
    {   
        float stepX = totalTimeX < (roundDuration / 2) ? totalTimeX / (roundDuration / 2) : 1 - (totalTimeX - roundDuration / 2) / (roundDuration / 2);        
        float stepY = totalTimeY < (roamingDuration / 2) ? totalTimeY / (roamingDuration / 2) : 1 - (totalTimeY - roamingDuration / 2) / (roamingDuration / 2);
        float stepZ = totalTimeZ < (roundDuration / 2) ? totalTimeZ / (roundDuration / 2) : 1 - (totalTimeZ - roundDuration / 2) / (roundDuration / 2);

        Vector3 position = transform.position;
        position.x = Mathf.SmoothStep(CenterRotation.x - roundRadius, CenterRotation.x + roundRadius, stepX);
        position.y = Mathf.SmoothStep(height - roamingRadius, height + roamingRadius, stepY);
        position.z = Mathf.SmoothStep(CenterRotation.z - roundRadius, CenterRotation.z + roundRadius, stepZ);
        transform.position = position;
        transform.rotation = Quaternion.LookRotation(CenterRotation - transform.position, Vector3.up);

        totalTimeX += Time.deltaTime;
        totalTimeZ += Time.deltaTime;
        totalTimeY += Time.deltaTime;
        totalTimeX = Repletion(totalTimeX, roundDuration);
        totalTimeY = Repletion(totalTimeY, roamingDuration);
        totalTimeZ = Repletion(totalTimeZ, roundDuration);

        if (totalTimeFov < fovDuration)
            totalTimeFov += Time.deltaTime;
        else
        {
            totalTimeFovDelay += Time.deltaTime;
            if (totalTimeFovDelay > fovDelay)
            {
                randomFov = roundRadiusStart / 2f + roundRadiusStart * UnityEngine.Random.Range(fovMin, fovMax);
                totalTimeFov = 0f;
                totalTimeFovDelay = 0f;
            }
        }
        float stepFov = totalTimeFov / fovDuration;
        roundRadius = Mathf.Lerp(roundRadius, randomFov, stepFov);
    }
    private float Repletion(float value, float valueMax)
    {
        if (value > valueMax)
            return value - valueMax;
        else
            return value;
    }
}
