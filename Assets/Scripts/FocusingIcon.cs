﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusingIcon : MonoBehaviour
{
    [SerializeField] public Canvas canvasMy;
    [SerializeField] public Transform TargetTransform;
    [SerializeField] public Vector3 Bias;

    private Vector2 ViewportPosition;
    private Vector2 WorldObject_ScreenPosition;
    private RectTransform CanvasRect;
    private RectTransform UI_Element;    
    
    private void Start()
    {
        UI_Element = GetComponent<RectTransform>();       
        if(canvasMy!=null)
            CanvasRect = canvasMy.GetComponent<RectTransform>();
    }

    void Update()
    {        
        if (TargetTransform != null && canvasMy != null)
        {
            Positioning(TargetTransform.position + Bias);
        }
    }
    private void Positioning(Vector3 pos)
    {
        ViewportPosition = Camera.main.WorldToViewportPoint(pos);

        WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        UI_Element.anchoredPosition = WorldObject_ScreenPosition;
    }
}
