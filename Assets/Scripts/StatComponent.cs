﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatComponent : HealthBar
{
    [SerializeField] private Image imageIcon;    
    public void SetImage(Sprite sprite)
    {
        imageIcon.sprite = sprite;
    }
}
