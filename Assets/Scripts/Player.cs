﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public Transform StatBuffContent;
    [SerializeField] public HealthBar HealthBar;
    public bool Death {get{ return HealthBar.Value <= 0; } }

    private StatComponent statHP;
    private Animator animator;
    private Player target;
    private List<Stat> stats;
    private List<Buff> buffs;

    private float axe = 0;
    private float meat = 0;
    private float armor = 0;
    private void Awake()
    {
        animator = GetComponent<Animator>();        
    }
    public void RestartSettings(Player target, List<Stat> stats, List<Buff> buffs, StatComponent statHP)
    {        
        this.target = target;
        this.stats = stats;
        this.buffs = buffs;
        this.statHP = statHP;
       
        foreach (var stat in stats)
        {
            switch (stat.id)
            {
                case 0:
                    statHP.Init(stat.value, 0, stat.value);
                    HealthBar.Init(stat.value, 0, stat.value);
                    animator.SetInteger("Health", HealthBar.GetPercent());
                    break;
                case 1:
                    armor = stat.value;
                    break;
                case 2:
                    axe = stat.value;
                    break;
                case 3:
                    meat = stat.value;
                    break;

            }
        }       
    }
    public void Attack()
    {
        if (target.Death || Death)
            return;
        animator.SetBool("Attack", true);
        float damageDone = target.ToDamage(axe);       
        Vampirism(damageDone);
    }
    public void Vampirism(float damageDone)
    {     
        if (damageDone > 0 && meat > 0)
            Healing(meat * damageDone / 100);
    }
    public void Healing(float hp)
    {        
        HealthBar.Value += hp;
        HealthBar.SetTextAnimator(hp);
        statHP.Value += hp;
    }
    public float ToDamage(float damage)
    {
        damage = damage * (100 - armor) / 100;

        HealthBar.Value -= damage;
        statHP.Value -= damage;
        HealthBar.SetTextAnimator(-damage);
        animator.SetInteger("Health", HealthBar.GetPercent());

        return damage;
    }
}
