﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFile
{    
    public static Sprite LoadSprite(string path) 
    { 
        return Resources.Load<Sprite>(path);
    }
}
