﻿using System.Collections.Generic;
using UnityEngine;
public class Pool<T> : MonoBehaviour where T:MonoBehaviour
{
    private List<T> objects = new List<T>();//pool
    private List<T> objectsOnStage = new List<T>();//на сцене
    private GameObject Prefab;
    private Transform Parent;
    public Pool(Transform parent, GameObject objPrefab)
    {
        Parent = parent;
        Prefab = objPrefab;
    }    
    public void Set(T obj)
    {
        obj.gameObject.SetActive(false);
        obj.transform.SetParent(Parent);
        objectsOnStage.Remove(obj);
        objects.Add(obj);
    }
    public T Get(Transform parent = null)
    {
        T obj = null;
        if (objects.Count == 0)
            obj = Instantiate(Prefab, parent == null ? Parent : parent).GetComponent<T>();
        else
        {
            obj = objects[objects.Count - 1];
            obj.transform.SetParent(parent == null ? Parent : parent);
            objects.RemoveAt(objects.Count - 1);
        }
        obj.gameObject.SetActive(true);
        obj.transform.SetAsLastSibling();//SetSiblingIndex(objectsOnStage.Count);
        //obj.transform.SetAsFirstSibling();
        objectsOnStage.Add(obj);
        return obj;
    }
    public void DeleteAll()
    {
        for (int i = objectsOnStage.Count - 1; i >= 0; i--)
        {
            Set(objectsOnStage[i]);
        }
    }
}