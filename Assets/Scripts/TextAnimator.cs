﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimator : Singleton<TextAnimator>
{
    private float speedStep = 0.05f;
    public void SetAnimateText(Text text, Color color, string value)
    {
        StartCoroutine(TextAnimation(text, color, value));
    }
    IEnumerator TextAnimation(Text text, Color color, string str)
    {
        Text textClone = Instantiate(text, transform);
        Vector2 startPosition = text.gameObject.transform.position;
        textClone.transform.position = startPosition;
        textClone.text = str;
        textClone.color = color;
        Vector2 endPosition = startPosition + new Vector2(0f, 100f);
        textClone.gameObject.SetActive(true);
        float value = 0;
        while (value < 1)
        {
            textClone.transform.position = Vector2.LerpUnclamped(startPosition, endPosition, value);
            value += speedStep;
            yield return new WaitForFixedUpdate();
        }
        //TODO необходим пул
        Destroy(textClone.gameObject);
    }
}
